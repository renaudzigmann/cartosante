# CartoSante

* Auteur : Renaud ZIgmann, renaud.zigmann@xsalto.com

## Introduction

Le développement PHP présent dans ce répertoire permet de présenter sous la forme d'un annuaire des données issues de OSM.

## Description

Les données présentées sont sélectionnées sur certaines thématiques et
sur certaines zones géographiques. De manière à obtenir des résultats
rapides, les résultats sont cachés dans des fichiers json.

## Installation

### Prérequis

Le logiciel développé a été conçu pour fonctionner en PHP à partir de
PHP7.1. Il peut utiliser une serveur OSM standard ou au contraire
exploiter un serveur OSM dédié.

Les données OSM sont accédées de deux manières :

* des interrogations en mode OverPass API
* l'utilisation des tuiles OSM

La configuration se trouve dans le fichier config.php avec les
constantes suivantes :

* OVERPASS : nom du serveur overpass api. Le serveur standard utilisé
  est http://overpass-api.de/api/interpreter
* TILES : schéma de l'url des tuiles utilisées. Les tuiles standard
  sont : https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
* ROOT_URL : adresse de l'application

### Configuration

La configuration des thématiques et des zones géographiques traitées
est réalisée dans le fichier config.json. Cette configuration est en
deux parties.

Les répertoires templates_c et tmp doivent être créés et vides.

### Configuration des thématiques

* id : chaine de caractères unique
* spec : chaine de sélection overpass de la thématique
* label : chaine de caractère "human readable"
* group : si la thématique appartient à un groupe de thématiques,
label du groupe de thématiques

### Configuration des zones géographiques

* label : nom en clair de la zone géographique
* spec : définition de la zone en terme de limite géographique, au
format overpass

### Cache

Les réponses sont conservées dans un cache côté serveur : pour que ce
cache soit recalculé en tâche de fond, il est impératif d'installer
une cron. Par exemple de la manière suivante pour un rafraichissement
du cache toutes les 30 minutes.

```
*/30 * * * *                 (cd ~/OSM/CartoSante/;nice -n 19 /usr/bin/php-cli cron.php) > /dev/null 2>&1
```

## Exécution

### En mode HTML classique

le script index.php présente un mode d'exécution "classique". 

```
<?php 
include "directory.php";

main('index.tpl');

?>
```

### en mode Widget

Le script examples/index.html présente une intégration en widget, qui
permet à ce paquet d'être utilisé dans un autre site internet, sans
retouche. Attention, certains sites peuvent avoir ajouté des sécurités
supplémentaires pour éviter le cross scripting. Des paramétrages
spécifiques peuvent être nécessaires.

Le script utilisé dans ce mode d'intégration est 'cross.php'.

Le principe du mode widget est d'intégrer sur la page d'un autre site
internet des éléments calculé sur le serveur cartosanté, à la volée.

Les CSS peuvent être personnalisées : la version de base fournie est
dans examples/css.  

## Questions réponses

### Comment effacer le cache

Suppression des fichiers temporaires du cache 

```
rm -rf tmp/*
```

### Mettre à jour les paquets composer


```
php-cli composer.phar update
```

### Modifier les gabarits d'affichage

Les gabarits d'affichage sont 

* index.tpl pour l'affichage en mode intégration 'classique'
* directory.tpl pour le fragment intégré en mode cross
* form.tpl intègre l'ensemble du formulaire
* header.tpl et bottom.tpl sont respectivement le haut de page et le bas de page en mode classique

Les gabarits d'affichage utilisent smarty (www.smarty.net)