</div>
</div>
<div id="footer">
  <div class="container">
  <hr>
    <p class="muted credit"><a href="https://www.adrets.asso.fr/"><img src="images/adrets.jpg"></a><a href="https://www.fondation-afnic.fr/"><img src="images/afnic.png"></a></p>
    <p class="muted credit"><a href="http://ferme.animacoop.net/wikis/afnicarto">Projet "CARTOGRAPHIE PARTICIPATIVE DES SERVICES"</a></p>
    <p class="muted credit">Développement sous licence GPLv3 par <a href="https://www.xsalto.com/">XSALTO</a></p>
  </div>
</div>


<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">
  jQuery(function () {

    jQuery('[data-toggle="popover"]').popover()
    jQuery('[data-toggle="tooltip"]').tooltip()


  });


</script>

</body>
</html>

