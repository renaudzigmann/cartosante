<div id="cartosante-widget-fragment">
<div id="cartosante-form-fragment">
{include file='form.tpl'}
</div>
<div id="cartosante-result-fragment">
{if isset($smarty.get.place)}
<div class="cv3-txtseul clearfix">
<h2>Résultat</h2>
{if empty($directory)}
<p>Il n'y a pas résultat</p>
{else}
<p>Vous pouvez aussi <A HREF="{$smarty.const.ROOT_URL}{$INTERFACE}?name={$smarty.request.name}&prof={$smarty.request.prof}&place={$smarty.request.place}&export=1">exporter ces résultats</a> au format CSV.
  <table class="table">
    <thead>
      <tr>
	<th>Nom</th>
	<th>Catégorie</th>
	<th colspan="2">Adresse</th>
	<th colspan="2">Ville</th>
	<th>Téléphone</th>
	<th>Site Internet</th>
	<th>Horaires d'ouverture</th>
	<th>Accès handicapé</th>
	{if $smarty.get.debug}
	<th>Vérification</th>
	{/if}
      </tr>
    </thead>
    <tbody>
      {foreach $directory as $entry}
      <tr>
      {if !empty($entry.lat) && !empty($entry.lon)}
	<td><a target="carte" href="https://www.openstreetmap.org/node/{$entry.osmid}#map=19/{$entry.lat}/{$entry.lon}">{$entry.name}</a></td>
	{else}
	<td><a target="carte" href="https://www.openstreetmap.org/way/{$entry.osmid}">{$entry.name}</a></td>
	{/if}
	<td>{$entry.cat}{if $entry.subcat}<br/>{$entry.subcat}{/if}</td>
	<td>{$entry.no}</td>
	<td>{$entry.street}</td>
	<td>{$entry.codepost}</td>
	<td>{$entry.city}</td>
	<td>{$entry.phone}</td>
	<td>{$entry.website}</td>
	<td>{$entry.opening_hours}{$entry.opening_hours1}</td>
	<td>{$entry.wheelchair}</td>
	{if $smarty.get.debug}
	<td>{$entry.checked}</td>
	{/if}
      </tr>
      {/foreach}
    </tbody>
  </table>
{/if}
</div>
{/if}
</div>
</div><!-- cartosante-widget -->

