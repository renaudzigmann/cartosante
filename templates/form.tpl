<div class="cv3-txtseul clearfix">
  <p>Cet annuaire est construit à partir de données <a href="https://www.openstreetmap.org/" target="carte">OpenStreetMap</a>. Tous les internautes peuvent contribuer à l'enrichissement et à la correction des informations, en les mettant à disposition de la communauté, en particulier grace <a href="https://www.mapcontrib.xyz/t/ab3524-Cartographier_les_services_de_sante">MapContrib</a>.
  </p>
  <h2>Recherche</h2>
    <form action="{$smarty.const.ROOT_URL}{$INTERFACE}" method="GET" class="form-horizontal" id="cartosante-form-id">
      <div class="form-group">
	<label for="Name" class="col-sm-2 control-label">Nom</label>
	<div class="col-sm-10">
	  <input type="text" name="name" value="{$smarty.get.name}"/>
	</div>
      </div>
      <div class="form-group">
	<label for="Ville" class="col-sm-2 control-label">Lieu</label>
	<div class="col-sm-10">
	  <select name="place">
	    {foreach $PLACES as $id=>$place}
	    <option value="{$id}" {if $smarty.get.place eq $id}selected{/if}>{$place.label}</option>
	    {/foreach}
	  </select>
	</div>
      </div>
      <div class="form-group">
	<label for="codepostal" class="col-sm-2 control-label">Catégorie</label>
	<div class="col-sm-10">
	  <select name="prof">
	    <option value="all" {if $smarty.get.prof eq "all"}selected{/if}>Tout</option>
	    {assign var="currentgroup" value=""}
	    {foreach $MED_SPECS as $id => $spec}
	    {if !empty($currentgroup) && $spec.group != $currentgroup}
	    {assign var="currentgroup" value=""}
            </optgroup>
            {/if}
            {if !empty($spec.group) && $spec.group != $currentgroup}
            <optgroup label="{$spec.group}">
            {assign var="currentgroup" value=$spec.group}
            {/if}
            <option value="{$spec.id}" {if $smarty.get.prof eq $spec.id}selected{/if}>{$spec.label}</option>
	    {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary">Chercher</button>
	</div>
      </div>      
   </form>
<script type="text/javascript">
jQuery('#cartosante-form-id').on('submit', function () {
jQuery.get(jQuery('#cartosante-form-id').attr('action'), jQuery('#cartosante-form-id').serialize(), 
function (data, textStatus) {
jQuery('#cartosante-widget').html(data);
});
return false;
});
</script>

{if isset($smarty.get.place)}
<div class="cv3-txtseul clearfix">
  <h2>Pour vous aider dans votre recherche</h2>
Dans le même lieu, vous pouvez chercher aussi
{foreach $MED_SPECS as $id => $spec}
{if $spec.found > 0}
<a href="{$smarty.const.ROOT_URL}{$INTERFACE}?prof={$spec.id}&place={$smarty.get.place}" onclick="jQuery('#cartosante-widget').load('h{$smarty.const.ROOT_URL}{$INTERFACE}?prof={$spec.id}&place={$smarty.get.place}');return false;">{$spec.label}({$spec.found})</a>&nbsp;
{/if}
{/foreach}
</div>
{/if}
</div>
