<?php

require_once('vendor/autoload.php');
require_once('config.php');

/// classe principale d'implémentation de l'annuaire
class OsmDirectory {
    public $directory=array();  /* liste des entrées de l'annuaires */
    public $fullcategories=array(); /* catégories de l'annuaire */
    private $_lifetime=60*60;       /* durée de vie du cache overpass */
    public $cronMode=false;
    
    function __construct() {
        $config=json_decode(file_get_contents('config.json'),true);
        assert(!empty($config), "Impossible de charger le fichier de configuration");
        $this->fullcategories = $config['thesaurus'];
        assert(!empty($this->fullcategories), "Catégories non renseignées dans le fichier de configuration");
        $this->places = $config['areas'];
        assert(!empty($this->places), "Lieux non renseignées dans le fichier de configuration");
    }

    /// execution de la requete overpass, cachée. Le cache est recalcul au bout de lifetime secondes.
    function getOverPassData($request) {
        $filename=$this->_getCacheFileName('ovp',$request);
        if(file_exists($filename)) {
            if ($file_date = filectime($filename)) {
                $age = (time()-$file_date);
                $lf=(integer)$this->_lifetime;
                if($age<$lf || !$this->cronMode)
                    return json_decode(file_get_contents($filename),true);
            }
        }
        $overpass = OVERPASS.'?data=[out:json];'.$request.';out;';
        $jsonout = json_decode(file_get_contents($overpass),true);
        file_put_contents($filename, json_encode($jsonout['elements']));
        // trace lors de l'exécution en tâche de fond
        echo $overpass.':'.count($jsonout['elements'])."\n";
        return $jsonout['elements'];
    }

    /// fonction de conversion des horaires d'ouverture
    // helper fonction for decodeOpeningHours
    private function codeToDay($code) {
        $daysLabels=['Mo'=>'lundi','Tu'=>'mardi','We'=>'mercredi','Th'=>'jeudi','Fr'=>'vendredi','Sa'=>'samedi','Su'=>'dimanche'];
        return $daysLabels[$code];
    }

    // helper fonction for decodeOpeningHours
    private function codeToHours($hours) {
        $hours=str_replace(':','h',$hours);
        $hours=str_replace('h00','h',$hours);
        $hours1=explode('-',$hours);
        return 'de '.$hours1[0].' à '.$hours1[1];
    }
    
    // transformation d'une chaine de codage opening hours de OSM en langage courant
    function decodeOpeningHours($oh) {
        if(empty($oh)) return '';

        $chunks = explode(';',$oh);
        $text='';
        // traitement des chaines de type Mo-Fr 07:30-19:00; Sa 07:30-12:00
        foreach($chunks as $chunk) {
            $chunk=trim($chunk); /* chaine type Mo-Fr 07:30-19:00,20:00-20:30 */
            if(!empty($text)) $text.='; ';
            if($chunk=='24/7') $text='24/7';
            $component=explode(' ',$chunk); /* chaine type Mo-Fr ou Fr */
            $days=explode('-',$component[0]);
            if(count($days)<=1) $text.='le '.$this->codeToDay($component[0]).' ';
            else {
                $text.='du '.$this->codeToDay($days[0]).' au '.$this->codeToDay($days[1]).' ';
            }
            $hours=explode(',',$component[1]); /* chaine type 07:30-19:00,20:00-20:30 */
            $hourstext='';
            if(count($hours)<=1) $hourstext.=$this->codeToHours($component[1]);
            else {
                foreach($hours as $hours1) {
                    /* chaine type 07:30-19:00 */
                    if(!empty($hourstext)) $hourstext.=',';
                    $hourstext.=' '.$this->codeToHours($hours1);
                }
            }
            $text.=$hourstext;
        }
        return $text;
    }


    // ajout des objets OSM au directory. Attention, on utilise l'id de
    // l'objet comme index du tableau pour garantir l'unicité
    // $directory : tableau annuaire de sortie
    // $order : tableau d'order
    // $elements : elements à ajouter dans l'annuaire
    function addToDirectory($elements) {
        $added = 0;
        foreach($elements as $o) {
            $tags=$o['tags'];
            if(!empty($tags['name'])) {
                $this->directory[$o['id']]=
                    array('name'=>$tags['name'],
                    'no'=>$tags['addr:housenumber'],
                    'street'=>$tags['addr:street'],
                    'codepost'=>$tags['addr:postcode'],
                    'city'=>$tags['addr:city'],
                    'phone'=>($tags['phone']??$tags['contact:phone']),
                    'website'=>$tags['website'],
                    'opening_hours'=>$this->decodeOpeningHours($tags['opening_hours']),
                    'wheelchair'=>$tags['wheelchair'],
                    'checked'=>$tags['survey:date'],
                    'cat'=>$this->findCategory($o),
                    'osmid'=>$o['id'],
                    'lat'=>$o['lat'],
                    'lon'=>$o['lon']);
                $added++;
            }
        }
        return $added;
    }

    function getPlaces() {
        return $this->places;
    }
    
    function testDirectory(&$hash, $elements) {
        foreach($elements as $o) {
            $tags=$o['tags'];
            if(!empty($tags['name'])) {
                $hash[(string)$o['id']]='1';
            }
        }
    }
    

    // chercher la catégorie de l'objet
    function findCategory($osmObject) {
        $tags=$osmObject['tags'];
        foreach($this->getFullCategories() as $o) {
            foreach($o['spec'] as $spec) {
                if('["amenity"="'.$tags['amenity'].'"]'==$spec) return $o['label'];
                if('["healthcare"="'.$tags['healthcare'].'"]'==$spec) return $o['label'];
                if('["healthcare:speciality"="'.$tags['healthcare:speciality'].'"]'==$spec) return $o['label'];
                if('["shop"="'.$tags['shop'].'"]'==$spec) return $o['label'];
                if($o['id']=='doctor' && '["amenity"="doctors"]'==$spec) return $o['label'];
                if($o['id']=='doctor' && '["healthcare"="doctor"]'==$spec) return $o['label'];
            }
        }
        return '';
    }


    // calcul du nom du fichier de cache
    function _getCacheFilename($prefix,$hash) {
        return 'tmp/'.$prefix.'-'.md5($hash).'.json';
    }

    /// rend la liste de toutes les catégories
    function getFullCategories() {
        return $this->fullcategories;
    }

    // recherche de toutes les categories disponibles, depuis le cache
    // ou par calcul
    function getCategories($place) {
        $cachefile = $this->_getCacheFilename('categ',$place);

        if(file_exists($cachefile)){
            $categories=json_decode(file_get_contents($cachefile),true);
        } else {
            $categories=$this->buildCategories($place);
            file_put_contents($cachefile,json_encode($categories));
        }
        return $categories;
    }

    /// calcul des categories disponibles sur le lieu par interrogation overpass
    function buildCategories($place) {
        $categories = $this->getFullCategories();
        if(empty($place)) return $categories;
        foreach($categories as $i=>$category) {
            $hash=[];
            foreach($category['spec'] as $spec) {
                $objects='node(area)'.$spec;
                $jsonout=$this->getOverpassData($place.$objects);
                $this->testDirectory($hash, $jsonout);
            }
            $categories[$i]['found']=count($hash);
        }
        return $categories;
    }


    /// calcul de l'annuaire pour la specialité et le lieu indiqués
    function getDirectory($prof, $place, $name=NULL) {
        if(!empty($this->directory)) return $this->directory;
    
        $categories = $this->getCategories($place);
                    
        foreach($categories as $i=>$category) {
            if($category['found']<=0) continue;
            if($category['id']==$prof || $prof=="all") {
                foreach($category['spec'] as $spec) {
                    $objects='node(area)'.$spec;
                    $jsonout=$this->getOverpassData($place.$objects);
                    if(!empty($name)) {
                        foreach($jsonout as $n=>$node) {
                            if(!preg_match('/('.$name.')/i', $node['tags']['name'])) {
                                unset($jsonout[$n]);
                            }
                        }
                    }
                    $this->addToDirectory($jsonout);
                }
            }
        }
        return $this->directory;
    }
}

/// calcul offline pour mettre les pages dans le cache
function cron() {
    $directory = new OsmDirectory();
    $directory->cronMode=true;
    foreach($directory->getPlaces() as $j=>$place) {
        echo "Processing ".$place['spec']."\n";
        $directory->buildCategories($place['spec']);
        $categories = $directory->getCategories($place['spec']);
        foreach($categories as $i=>$category) {
            if($category['found']<=0) continue;
            $url=ROOT_URL.'index.php?name=&place='.$j.'&prof='.$category['id'];
            echo "Processing ".$url."\n";
            file_get_contents($url);
        }
        $url=ROOT_URL.'index.php?name=&place='.$j.'&prof=all';
        echo "Processing ".$url."\n";
        file_get_contents($url);
    }
}


/// point d'entré principal
function main($template='index.tpl') {

    $smarty = new Smarty();
    $smarty->setTemplateDir(__DIR__.'/templates/');
    $smarty->setCompileDir(__DIR__.'/templates_c/');
    $smarty->setConfigDir(__DIR__.'/configs/');
    $smarty->setCacheDir(__DIR__.'/cache/');
    $place=$_GET['place'];
    $prof=$_GET['prof'];
    $name=$_GET['name'];
    $export=$_GET['export'];

    // deux cas : soit export soit affichage
    $directory = new OsmDirectory();
    $places=$directory->getPlaces();
    if(empty($place)) $place="0";
    $place=$places[$place]['spec'];
    $categories = $directory->getCategories($place);
    if(!empty($prof) && !empty($place)) {
        $localdirectory = $directory->getDirectory($prof, $place, $name);
    }
    
    $smarty->assign('directory', $localdirectory);
    $smarty->assign('MED_SPECS', $categories);
    $smarty->assign('PLACES', $places);
    $smarty->assign('INTERFACE', $_SERVER['PHP_SELF']);
    $smarty->assign('ROOT', ROOT_URL);

    if(isset($template)) $smarty->display($template);
    elseif(empty($export)) {
        $smarty->display('directory.tpl');
    } else {
        $content=$smarty->fetch('export.tpl');
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=export.csv');
        header('Content-length: ' . strlen($content));
        echo $content;
    }
}

?>
