<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb1a82bd0f8e357ed037e9b89f453c800
{
    public static $files = array (
        'f084d01b0a599f67676cffef638aa95b' => __DIR__ . '/..' . '/smarty/smarty/libs/bootstrap.php',
    );

    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'PEAR' => 
            array (
                0 => __DIR__ . '/..' . '/pear/pear_exception',
            ),
        ),
        'L' => 
        array (
            'Log' => 
            array (
                0 => __DIR__ . '/..' . '/pear/log',
            ),
        ),
        'H' => 
        array (
            'HTTP_Request2' => 
            array (
                0 => __DIR__ . '/..' . '/pear/http_request2',
            ),
        ),
    );

    public static $classMap = array (
        'Net_URL2' => __DIR__ . '/..' . '/pear/net_url2/Net/URL2.php',
        'Services_OpenStreetMap' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap.php',
        'Services_OpenStreetMap_API_V06' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/API/V06.php',
        'Services_OpenStreetMap_Changeset' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Changeset.php',
        'Services_OpenStreetMap_Changesets' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Changesets.php',
        'Services_OpenStreetMap_Comment' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Comment.php',
        'Services_OpenStreetMap_Comments' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Comments.php',
        'Services_OpenStreetMap_Config' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Config.php',
        'Services_OpenStreetMap_Criterion' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Criterion.php',
        'Services_OpenStreetMap_Exception' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Exception.php',
        'Services_OpenStreetMap_InvalidArgumentException' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/InvalidArgumentException.php',
        'Services_OpenStreetMap_Node' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Node.php',
        'Services_OpenStreetMap_Nodes' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Nodes.php',
        'Services_OpenStreetMap_Nominatim' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Nominatim.php',
        'Services_OpenStreetMap_Note' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Note.php',
        'Services_OpenStreetMap_Notes' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Notes.php',
        'Services_OpenStreetMap_Object' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Object.php',
        'Services_OpenStreetMap_Objects' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Objects.php',
        'Services_OpenStreetMap_OpeningHours' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/OpeningHours.php',
        'Services_OpenStreetMap_Relation' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Relation.php',
        'Services_OpenStreetMap_Relations' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Relations.php',
        'Services_OpenStreetMap_RuntimeException' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/RuntimeException.php',
        'Services_OpenStreetMap_Transport' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Transport.php',
        'Services_OpenStreetMap_Transport_HTTP' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Transport/HTTP.php',
        'Services_OpenStreetMap_Transport_HTTPCached' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Transport/HTTPCached.php',
        'Services_OpenStreetMap_User' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/User.php',
        'Services_OpenStreetMap_Way' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Way.php',
        'Services_OpenStreetMap_Ways' => __DIR__ . '/..' . '/kenguest/services-openstreetmap/Services/OpenStreetMap/Ways.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInitb1a82bd0f8e357ed037e9b89f453c800::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitb1a82bd0f8e357ed037e9b89f453c800::$classMap;

        }, null, ClassLoader::class);
    }
}
